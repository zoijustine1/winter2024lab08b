import java.util.Random;
public class Board {
	
	//create fields
	private Tile[][] grid;
	private final int SIZE =5;
	
	//create constructors which uses a nested loops to pupulate or initialize the values inside the 2d array to be blank and hidden wall
	public Board(){
		//or this SIZE = 3;
		Random rng = new Random();
		this.grid = new Tile[this.SIZE][this.SIZE];
		for(int i=0;i<this.grid.length;i++){
			int randNum = rng.nextInt(grid[i].length);
			for(int j=0;j<this.grid[i].length;j++){
				//this.grid[i][j] = Tile.BLANK;
				if(j == randNum){
					this.grid[i][j] = Tile.HIDDEN_WALL;
				}
				else {
					this.grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	
	//create a toString method that returns a string that uses the Tile class to replace the blank values into the input constructor of the Tile class.
	public String toString(){
		String builder ="";
		for(int i=0;i<this.grid.length;i++){
			for(int j=0;j<this.grid[i].length;j++){
				builder += grid[i][j].getName() + " ";
			}
			builder += "\n";
		}
		return builder;
	}
	
	//create a placeToken method that takes 2 parameters and returns an int. This method checks if the row and column is betweem 0 and 4 and checks the condition and returns an int.
	public int placeToken(int row, int column){
		boolean checker = (row < 0 || row >= grid.length || column < 0 || column >= grid.length);
		if(checker){
			return -2;
		}
		else if(this.grid[row][column] == Tile.CASTLE || this.grid[row][column] == Tile.WALL){
			return -1;
		}
		else if(this.grid[row][column] == Tile.HIDDEN_WALL){
			this.grid[row][column] = Tile.WALL;
			return 1;
		}
		else {
			this.grid[row][column] = Tile.CASTLE;
			return 0;
		}
	}
}