import java.util.Scanner;
public class BoardGameApp {
	public static void main(String[] args){
		runGame();
		
	}
	//this method goes in a loop while numCastles is greater than 0 and turns is less than 8. This method asks the user to input a row and a column and calls
	// the placeToken method inside the board game to check if it returns negative, it will asks the user to enter a new row or column. If it returns 1, turns will increase by 1
	// if it is equal to 0, turns increases and numCastles decreases by 1. At the end, if the numCastles is equal to 0, the player wins; otherwise, player loses.
	public static void runGame() {
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to a simple board game with 2D arrays");
		int numCastles = 7;
		int turns = 0;
		Board board = new Board();
		while(numCastles > 0 && turns < 8){
			System.out.println(board);
			System.out.println("Number of numCastles: " + numCastles);
			System.out.println("Number of turns: " + turns);
			System.out.println("Enter a row");
			int row = reader.nextInt();
			System.out.println("Enter a column");
			int column = reader.nextInt();
			int checker = board.placeToken(row,column);
			while(checker < 0){
				System.out.println("Invalid row or column. Enter a new and column");
				row = reader.nextInt();
				column = reader.nextInt();
				checker = board.placeToken(row,column);
			}
			if(checker == 1){
				System.out.println("There was a wall at that position");
				turns++;
			}
			else if(checker == 0){
				System.out.println("A castle tile was successfully placed");
				turns++;
				numCastles--;
			}
		}
		System.out.println(board);
		if(numCastles == 0){
			System.out.println("You won");
		}
		else {
			System.out.println("You lose");
		}
		
	}
}